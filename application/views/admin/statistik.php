<?php include('base.php') ?>

<?php startblock('title') ?>
	<?= $title ?>
<?php endblock() ?>

<?php startblock('isi') ?>
	<div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Statistik Pengaduan Bulanan</h4>

                    <div class="flot-chart flot-bar"></div>
                    <div class="flot-chart-legends flot-chart-legends--bar"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        	<div class="card">
                <div class="card-body">
                    <h4 class="card-title">Pengaduan Chart</h4>
                    <div class="flot-chart flot-pie"></div>
                    <div class="flot-chart-legends flot-chart-legend--pie"></div>
                </div>
            </div>
        </div>
	</div>
<?php endblock() ?>

<?php startblock('js') ?>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/Flot/jquery.flot.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/Flot/jquery.flot.pie.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<?php endblock() ?>

<?php startblock('custom_js')?>	
	<script type="text/javascript">
		var barChartData = []
		$(document).ready(function(){
			var pieData = [
		        {data: <?= $jumlah_pengaduan_belum ?>, color: '#ff6b68', label: 'Pengaduan belum tertangani'},
		        {data: <?= $jumlah_pengaduan_proses ?>, color: '#03A9F4', label: 'Sedang diproses'},
		        {data: <?= $jumlah_pengaduan_selesai ?>, color: '#32c787', label: 'Selesai diproses'},
		        // {data: 5, color: '#d066e2', label: 'Daihatsu'}
		    ];

		    if($('.flot-pie')[0]){
		        $.plot('.flot-pie', pieData, {
		            series: {
		                pie: {
		                    show: true,
		                    stroke: {
		                        width: 2
		                    }
		                }
		            },
		            legend: {
		                container: '.flot-chart-legend--pie',
		                backgroundOpacity: 0.5,
		                noColumns: 0,
		                backgroundColor: "white",
		                lineWidth: 0,
		                labelBoxBorderColor: '#fff'
		            }
		        });
		    }

		    var barChartOptions = {
		        series: {
		            bars: {
		                show: true,
		                barWidth: 0.05,
		                fill: 1
		            }
		        },
		        grid : {
		            borderWidth: 1,
		            borderColor: '#f8f8f8',
		            show : true,
		            hoverable : true,
		            clickable : true
		        },
		        yaxis: {
		            tickColor: '#f8f8f8',
		            tickDecimals: 0,
		            font :{
		                lineHeight: 13,
		                style: "normal",
		                color: "#9f9f9f",
		            },
		            shadowSize: 0
		        },
		        xaxis: {
		            tickColor: '#fff',
		            tickDecimals: 0,
		            font :{
		                lineHeight: 13,
		                style: "normal",
		                color: "#9f9f9f"
		            },
		            shadowSize: 0,
		            ticks: [[1, 'Januari'], [2, 'Februari'], [3, 'Maret'], [4, 'April'], [5, 'Mei'], [6, 'Juni'], [7, 'Juli'], [8, 'Agustus'], [9, 'September'], [10, 'Oktober'], [11, 'November'], [12, 'Desember']]
		            // mode: 'time'
		        },
		        legend:{
		            container: '.flot-chart-legends--bar',
		            backgroundOpacity: 0.5,
		            noColumns: 0,
		            backgroundColor: '#fff',
		            lineWidth: 0,
		            labelBoxBorderColor: '#fff'
		        },
		        hoverable: true
		    };

		    colorbar = ['#32c787', '#03A9F4', '#e91d1d', '#f5c942'];

			$.ajax({
				url: '<?= base_url() ?>admin/pengaduan/getstatistik',
				type: 'GET',
				success: function(respon){
					respon = JSON.parse(respon)
					if(respon){
						for (var i = 0; i < respon.length; i++){
							var dataku = {
								            label: respon[i].nama_jenis,
								            data: respon[i].data,
								            color: colorbar[i],
								            bars: {
								                order: i
								            }
								        }
							barChartData.push(dataku)
							if ($('.flot-bar')[0]) {
						        $.plot($('.flot-bar'), barChartData, barChartOptions);
						    }
						}
					}
				}
			})
		});
	</script>
<?php endblock() ?>

