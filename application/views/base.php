<?php require_once('ti.php'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php emptyblock('title') ?> | PM (Pengaduan Masyarakat)</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/lightgallery/dist/css/lightgallery.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <?php emptyblock('css') ?>
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/css/app.min.css">
        <?php emptyblock('custom_css') ?>
        <style type="text/css">
            .float{
                position:fixed;
                bottom:40px;
                right:40px;
            }
        </style>
        <?php 
            $user_saya = NULL;
            if($this->session->userdata('frontend_logged_in'))
                $user_saya = $this->session->userdata('result'); 
        ?>
    </head>

    <body data-ma-theme="green">
        <main class="main main--alt">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>

            <header class="header">
                <div class="header__logo">
                    <h1><a href="<?= base_url() ?>">Pengaduan Masyarakat</a></h1>
                </div>

                <ul class="top-nav">
                    <li>
                        <a href="<?= base_url() ?>">Beranda</a>
                    </li>
                    <?php if(!$user_saya){ ?>
                        <li>
                            <a href="<?= base_url('daftar') ?>">Daftar</a>
                        </li>
                        <li>
                            <a href="<?= base_url('login') ?>">Login</a>
                        </li>
                    <?php }else{ ?>
                        <li>
                            <a href="<?= base_url('logout') ?>">Logout</a>
                        </li>
                    <?php } ?>
                </ul>
            </header>

            <section class="content content--full">
                <div class="content__inner">
                    <?php emptyblock('content') ?>
                    <footer class="footer hidden-xs-down">
                        <p>© Material Admin Responsive. All rights reserved.</p>

                        <ul class="nav footer__nav">
                            <a class="nav-link" href="#">Homepage</a>

                            <a class="nav-link" href="#">Company</a>

                            <a class="nav-link" href="#">Support</a>

                            <a class="nav-link" href="#">News</a>

                            <a class="nav-link" href="#">Contacts</a>
                        </ul>
                    </footer>
                </div>
            </section>
        </main>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/lightgallery/dist/js/lightgallery-all.min.js"></script>
        <?php emptyblock('js') ?>
        <script src="<?= base_url() ?>static/admin/js/app.min.js"></script>
        <?php emptyblock('custom_js') ?>
    </body>
</html>