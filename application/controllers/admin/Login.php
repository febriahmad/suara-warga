<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->library('form_validation');
	}

	public function index(){
		if($this->session->userdata('logged_in')){
			redirect(base_url("admin"), "refresh");
		}
		$this->load->view('admin/login');
	}

	function login(){
		$data['sukses'] = false;
		$this->form_validation->set_rules('username','NIK/Email','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run() != false){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$this->db->where('nik', $username);
			$this->db->or_where('email', $username);
			$this->db->where('password', md5($password));
			$query = $this->db->get('akun');
			if($query->num_rows() > 0){
				$result = $query->result();
				$result = $result[0];
				$data['sukses'] = true;			
				$data['result'] = $result;
				unset($result->password); // untuk membuang field password dari array
				$field = array(
					'logged_in' => TRUE,
					'result' => $result
				);
				$this->session->set_userdata($field);
			}else{	
				$data['error'] = '<p>Email dan Password tidak ditemukan di dalam SISTEM.</p>';
			}
		}else{
			$data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url("admin/login"), "refresh");
	}
}